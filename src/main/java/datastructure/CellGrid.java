package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	private int rows;
	private int columns;
	private CellState[][] cellstate;

    public CellGrid(int rows, int columns, CellState initialState) {
    	this.rows = rows;
    	this.columns = columns;
    	this.cellstate = new CellState[rows][columns];
    	for (int row = 0; row < this.rows; row++) {
			for (int col = 0; col < this.columns; col++) {
				set(row, col, initialState);
			}
		}
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        cellstate[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return cellstate[row][column];
    }

    @Override
    public IGrid copy() {
    	CellGrid copy = new CellGrid(this.rows, this.columns, CellState.ALIVE);
    	for (int row = 0; row < this.rows; row++) {
			for (int col = 0; col < this.columns; col++) {
				copy.set(row, col, get(row, col));
			}
		}
        return copy;
    }
    
}
